#!/bin/bash

MOMENTUM=270
PARTICLE=alpha
ifilename1=${PARTICLE}_
ifilename2=_MeV_C2
rfilename1=${PARTICLE}_
rfilename2=_MeV_C2.xml

while [ $MOMENTUM -lt 410 ]; do

  ifilename=$ifilename1$MOMENTUM$ifilename2
  echo Creating input file: input/$ifilename
  
  /u/group/clas12-alert/users/mathieu/c12sim_analysis/create_c12sim_macro -p $PARTICLE -P $MOMENTUM -n 10000 >> /u/group/clas12-alert/users/mathieu/c12sim_analysis/input/$ifilename
  
  rfilename=$rfilename1$MOMENTUM$rfilename2
  echo Creating request file: sub/$rfilename

  echo "<Request>"                                                                                                                                                    >> sub/$rfilename
  echo "  <Name name=\"c12sim_${PARTICLE}_${MOMENTUM}\"/>"                                                                                                            >> sub/$rfilename
  echo "  <Project name=\"clas12\"/>"                                                                                                                                 >> sub/$rfilename
  echo "  <Track name=\"debug\"/>"                                                                                                                                    >> sub/$rfilename
  echo "  <DiskSpace space=\"100\" unit=\"GB\"/>"                                                                                                                     >> sub/$rfilename
  echo "  <Memory space=\"2\" unit=\"GB\"/>"                                                                                                                          >> sub/$rfilename
  echo "  <Command><![CDATA["                                                                                                                                         >> sub/$rfilename
  echo "echo \"Sourcing Env Setup\""                                                                                                                                  >> sub/$rfilename
  echo "source /u/home/ehrhart/ANL/farm/env_script.csh"                                                                                                               >> sub/$rfilename
  echo "echo \"Start Simulation\""                                                                                                                                    >> sub/$rfilename
  echo "/u/group/clas12-alert/users/mathieu/c12sim_analysis/c12sim --simple-eg=1 -b -D . < /u/group/clas12-alert/users/mathieu/c12sim_analysis/input/${ifilename}"    >> sub/$rfilename
  echo "echo \"End Simulation\""                                                                                                                                      >> sub/$rfilename
  echo "  ]]>"                                                                                                                                                        >> sub/$rfilename
  echo "  </Command>"                                                                                                                                                 >> sub/$rfilename
  echo "  <Job>"                                                                                                                                                      >> sub/$rfilename
  echo "    <Input src=\"file:/u/group/clas12-alert/users/mathieu/c12sim_analysis/input/$ifilename\" dest=\"$ifilename\"/>"                                           >> sub/$rfilename
  echo "    <Output src=\"clas12sim0.root\" dest=\"file:/u/group/clas12-alert/users/mathieu/c12sim_analysis/data/rootfiles/c12sim_${PARTICLE}_${MOMENTUM}.root\" /> " >> sub/$rfilename
  echo "  </Job>"                                                                                                                                                     >> sub/$rfilename
  echo "</Request>"                                                                                                                                                   >> sub/$rfilename
  echo Submitting Job:

  jsub -xml sub/$rfilename

  let MOMENTUM=MOMENTUM+10

done

jobstat -u ehrhart
