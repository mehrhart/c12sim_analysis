set var = `pwd`

echo "$var"

cd /u/group/clas12-alert/local/bin/

source geant4.csh
source thisroot.csh

setenv PATH $PATH\:/u/group/clas12-alert/local/bin
setenv LD_LIBRARY_PATH $LD_LIBRARY_PATH\:/u/group/clas12-alert/local/lib:/group/clas12-alert/local/lib64
setenv PYTHONPATH $PYTHONPATH\:/group/clas12-alert/local/lib
setenv MANPATH $MANPATH\:/group/clas12-alert/local/man:/group/clas12-alert/local/share/man

cd var
